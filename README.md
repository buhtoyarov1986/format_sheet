# Format
The script formats the sheet.

## Getting started

```ruby
rvm use 2.5.1
bundle install

# By default, the output file is saved on the desktop
./format.rb ~/Downloads/data.xlsx

# You can specify the path to save the output file.
./format.rb ~/Downloads/data.xlsx ~/Downloads/output_data.xlsx
```