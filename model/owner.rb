class Owner
  include Virtus.model
  include SpreadsheetArchitect

  attribute :owner_care_of_name, String
  attribute :onwer_one_first_name, String
  attribute :onwer_one_last_name, String
  attribute :onwer_two_first_name, String
  attribute :onwer_tow_last_name, String

  attribute :mail_address, String
  attribute :mail_city, String
  attribute :mail_state, String
  attribute :mail_zip, String
  attribute :property_address, String

  def spreadsheet_columns
    [:name, :mail_address, :mail_city, :mail_state, :mail_zip, :property_address]
  end

  def name
    if owner_care_of_name
      return owner_care_of_name
    end
    
    if onwer_one_first_name && onwer_two_first_name
      return [onwer_one_first_name.capitalize, onwer_two_first_name.capitalize].join(' & ')
    end

    if !onwer_two_first_name && onwer_one_first_name
      return [onwer_one_first_name.capitalize, onwer_one_last_name.capitalize].join(' ')
    end
  end

  def mail_address
    titleize(super.to_s)
  end

  def mail_state
    titleize(super.to_s)
  end

  private

  def titleize(value)
    value.to_s.split(' ').map(&:capitalize).join(' ')
  end
end