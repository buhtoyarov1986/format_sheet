class Sheet
  include Virtus.model
  
  attribute :owners, OwnerCollection[Owner]
end