class OwnerCollection < Array
  def <<(owner)
    if owner.kind_of?(Hash)
      super(Owner.new(owner))
    else
      super
    end
  end  
end