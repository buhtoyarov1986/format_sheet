#!/usr/bin/env ruby

require 'roo'
require 'spreadsheet_architect'
require 'virtus'

require_relative 'model/owner_collection'
require_relative 'model/owner'
require_relative 'model/sheet'

FIELDS = {
  owner_care_of_name: /OWNER CARE OF NAME/i, 
  onwer_one_first_name: /OWNER 1 FIRST NAME/i,
  onwer_one_last_name: /OWNER 1 LAST NAME/i,
  onwer_two_first_name: /OWNER 2 FIRST NAME/i,
  onwer_tow_last_name: /OWNER 2 LAST NAME/i,
  mail_address: /Mail Address/i,
  mail_city: /MAIL CITY/i,
  mail_state: /Mail State/i,
  mail_zip: /MAIL ZIP CODE/i,
  property_address: /PROPERTY ADDRESS/i
}

input_file_path = ARGV[0]
output_file_path = File.expand_path(ARGV[1] || '~/Desktop/sheet.xlsx')

xlsx = Roo::Spreadsheet.open(input_file_path)
sheet = Sheet.new

xlsx.sheet(0).each(FIELDS).drop(1).each do |hash|
  sheet.owners << hash
end

file_data = SpreadsheetArchitect.to_xlsx(instances: sheet.owners)

File.open(output_file_path, 'w+b') do |f|
  f.write file_data
end
